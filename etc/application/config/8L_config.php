<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['APP_TITLE'] = "CI-TEMPLATE-BARE";

$config['HASH'] = "uMMcgjnOELIa6oydRivPkiMrBG8.aFp.";   // DO NOT MODIFY OR DELETE
$config['8L_URL'] = "http://".$_SERVER['HTTP_HOST'];

$config['SCRIPTS'] = $config['8L_URL'] . "/public/scripts";
$config['STYLESHEETS'] = $config['8L_URL'] . "/public/stylesheets";
$config['IMAGES'] = $config['8L_URL'] . "/public/images";

